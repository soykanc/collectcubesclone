﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectorMove : MonoBehaviour
{
    private Touch touchCollector;
    
    [SerializeField]
    public float speed;

    void start()
    {
        speed = 0.1f;       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touchCollector = Input.GetTouch(0);
            if (touchCollector.phase == TouchPhase.Moved)
            {
                transform.position = new Vector3(transform.position.x+touchCollector.deltaPosition.x*speed,
                    transform.position.y,
                    transform.position.z+touchCollector.deltaPosition.y*speed);
            }
        }
        if (Input.GetMouseButton(0))
        {
            transform.position = new Vector3(transform.position.x+0.05f * speed,
                    transform.position.y,
                    transform.position.z + 0.05f * speed);
        }
    }
}
