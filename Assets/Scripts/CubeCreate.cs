﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCreate : MonoBehaviour
{
    public Texture2D addImage;
    public GameObject cube;
    public List<Texture2D> imageList;
    public Transform parentTransform;
    public static Color[] tempImage;
    void Start()
    {
        //addImage.Resize(128, 128);
        for (int i = 0; i < addImage.width; i++)
        {
            for (int j = 0; j < addImage.height; j++)
            {
                Color pixel = addImage.GetPixel(i, j);
                if (pixel.a == 0)
                {

                }
                else
                {
                    GameObject obje = Instantiate(cube, new Vector3(i * 0.05f, j * 0.05f, 0), Quaternion.identity);
                    obje.transform.position=parentTransform.position;
                    obje.GetComponent<MeshRenderer>().material.color = pixel;
                    Debug.Log(" i " + i + " j " + j);
                }
            }
        }


        //    for (int k = 0; k < 96; k++)
        //    {
        //        for (int l = 0; l < 64; l++)
        //        {
        //            tempImage[1]= imageList[1].GetPixel(k+3, l+2);
        //            if (tempImage[1].a != 0)
        //            {
        //                GameObject cubesnew = Instantiate(cube, (new Vector3(k * 0.3f, l * 0.3f, 0)),Quaternion.identity);
        //                cubesnew.transform.SetParent(parentTransform);
        //                cubesnew.GetComponent<MeshRenderer>().material.color = tempImage[1];
        //            }
        //        }

        //    }

        //}

        // Update is called once per frame
        void Update()
        {

        }
    }
}
